# Campost #

Campost is a public-facing webcam aggregation site created with Meteor.js.
It was created for HopHacks in fall 2014.  HopHacks is a student run hackathon at John Hopkins University.

Campost uses a voting system to keep the most popular webcams at the top of the site.
The site updates in real time as votes are submitted by users.